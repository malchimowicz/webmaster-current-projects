<?php

// configure
$from = 'Kontakt z webma.pl <webma@webma.com>';
$sendTo = 'Wyślij do <michalalchimowicz@gmail.com>';
$subject = 'Nowa wiadomość od';
$fields = array('name' => 'Name', 'surname' => 'Surname', 'phone' => 'Phone', 'email' => 'Email', 'message' => 'Message'); // array variable name => Text to appear in email
$okMessage = 'Formularz kontaktowy został wysłany pomyślnie!'; 
$errorMessage = 'Podczas wysyłania formularza wystąpił błąd. Spróbuj ponownie!';  

// let's do the sending

try
{
    $emailText = "Otrzymałeś nową wiadomość od\n=============================\n";

    foreach ($_POST as $key => $value) {

        if (isset($fields[$key])) {
            $emailText .= "$fields[$key]: $value\n";
        }
    }

    mail($sendTo, $subject, $emailText, "From: " . $from);

    $responseArray = array('type' => 'success', 'message' => $okMessage);
}
catch (\Exception $e)
{
    $responseArray = array('type' => 'danger', 'message' => $errorMessage);
}

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);
    
    header('Content-Type: application/json');
    
    echo $encoded;
}
else {
    echo $responseArray['message'];
}