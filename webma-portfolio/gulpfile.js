var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
	autoprefixer = require('gulp-autoprefixer'),
	uglifycss = require('gulp-uglifycss');

gulp.task("concat",function(){
	return gulp.src(['js/jquery.scrollTo.min.js','js/scroll.js'])
	.pipe(concat('jsscroll.js'))
	.pipe(gulp.dest('dist'))
})

gulp.task("uglify",function(){
	return gulp.src('dist/jsscroll.js')
	.pipe(uglify())
	.pipe(gulp.dest('dist'));
})

gulp.task("imagemin",function(){
	return gulp.src('img/*')
	.pipe(imagemin())
	.pipe(gulp.dest('dist/img'));
})

gulp.task("imagemin",function(){
	return gulp.src('icon1/*')
	.pipe(imagemin())
	.pipe(gulp.dest('dist/icon1'));
})

gulp.task("autoprefixer",function(){
	return gulp.src('css/style.css')
	.pipe(autoprefixer())
	.pipe(gulp.dest('dist/'));
})

gulp.task("uglifycss",function(){
	return gulp.src('css/style.css')
	.pipe(uglifycss())
	.pipe(gulp.dest('css'));
})

gulp.task("concat",function(){
	return gulp.src(['js/validator.js','js/contact.js'])
	.pipe(concat('jsval-con.js'))
	.pipe(gulp.dest('dist'))
})

gulp.task("uglify",function(){
	return gulp.src('dist/jsval-con.js')
	.pipe(uglify())
	.pipe(gulp.dest('dist'));
})