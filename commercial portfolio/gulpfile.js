var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
	uglifycss = require('gulp-uglifycss');

gulp.task("concat",function(){
	return gulp.src(['css/foundation.css','css/app.css'])
	.pipe(concat('cssall.css'))
	.pipe(gulp.dest('dist'))
})

gulp.task("image",function(){
	return gulp.src('img1/*')
	.pipe(imagemin())
	.pipe(gulp.dest('dist/img1'));
})


gulp.task("uglify",function(){
	return gulp.src('dist/final1.js')
	.pipe(uglify())
	.pipe(gulp.dest('dist'));
})

gulp.task("uglifycss",function(){
	return gulp.src('css/cssall.css')
	.pipe(uglifycss())
	.pipe(gulp.dest('css'));
})